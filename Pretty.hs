module Pretty where

    import Type( Goal(..), Prog(..), Rule(..), Term(..), VarName(VarName) );

    -- class type for pretty-print
    class Pretty a where
        pretty :: a -> String

    -- returns pretty-printable term as string
    instance Pretty Term where
        pretty (Var (VarName name)) = name
        
        pretty (Comb name []) = name
        -- pretty (Comb "." (x:xs)) = "[" ++ pretty x ++ foldr (\x1 _ -> "|" ++ pretty x1) "" xs ++ "]"
        pretty (Comb name (x:xs)) = name ++ "(" ++ pretty x ++ foldr (\x1 y -> ", " ++ pretty x1 ++ y) "" xs ++ ")"

    -- returns pretty-printable rule as string
    instance Pretty Rule where
        pretty (Rule t []) = pretty t ++ "."
        pretty (Rule t (x:xs)) = pretty t ++  " :- " ++ pretty x ++ foldr (\x1 y -> ", " ++ pretty x1 ++ y) "" xs ++ "."

    -- returns pretty-printable prog as string
    instance Pretty Prog where
        pretty (Prog []) = ""
        pretty (Prog (x:xs)) = pretty x ++ foldr (\x1 y -> "\n" ++ pretty x1 ++ y) "" xs

    -- returns pretty-printable goal as string
    instance Pretty Goal where
        pretty (Goal []) = "?- ."
        pretty (Goal (x:xs)) = "?- " ++ pretty x ++ foldr (\x1 y -> ", " ++ pretty x1 ++ y) "" xs ++ "."
