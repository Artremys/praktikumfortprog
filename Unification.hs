{-# LANGUAGE TemplateHaskell #-}
module Unification (unify, ds, runTests) where
    import Type ( Term(..), VarName(VarName) );
    import Vars ( Vars(allVars) )
    import Subst ( Subst, domain, empty, single, apply, compose )
    import Test.QuickCheck ( quickCheckAll, (==>), Property )
    import Data.Maybe ( isJust, fromJust )

    -- calculates the disagreement set ds(t,t')
    ds :: Term -> Term -> Maybe (Term, Term)
    ds (Var n1) b = if Var n1 == b || n1 == VarName "_" then Nothing else Just (Var n1, b)
    ds b (Var n1) = if Var n1 == b || n1 == VarName "_" then Nothing else Just (b, Var n1)
    ds (Comb c ts) (Comb c2 ts2) = do
        if c /= c2 || length ts /= length ts2
            then Just (Comb c ts, Comb c2 ts2)
            else
                do
                    firstNotEqual ts ts2
                    where
                        isEqualVars :: Term -> Term -> Bool
                        isEqualVars (Var v1) (Var v2) = v1 == v2 || v1 == VarName "_" || v2 == VarName "_"
                        isEqualVars _ _ = False

                        firstNotEqual :: [Term] -> [Term] -> Maybe (Term, Term)
                        firstNotEqual [] [] = Nothing
                        firstNotEqual [x] [y] = do
                            if isEqualVars x y
                                then Nothing
                                else ds x y
                        firstNotEqual (t1:ts1) (t2:tsm2) =
                            do
                                let firstElemRes = firstNotEqual [t1] [t2]
                                if isJust firstElemRes
                                    then firstElemRes
                                    else firstNotEqual ts1 tsm2
                        firstNotEqual _ _ = undefined

    -- determines the most general unifier (mgu)
    unify :: Term -> Term -> Maybe Subst
    unify  = mgu empty
        where
            mgu subst t1 t2 = case ds (apply subst t1) (apply subst t2) of
                                    Just (Var x, b) -> if x `elem` allVars b then Nothing else mgu (compose (single x b) subst) t1 t2
                                    Just (b, Var x) -> if x `elem` allVars b then Nothing else mgu (compose (single x b) subst) t1 t2
                                    Just _ -> Nothing
                                    Nothing -> Just subst

    -- start tests
    prop_test1 :: Term -> Bool
    prop_test1 t = ds t t == Nothing

    prop_test2 :: Term -> Term -> Property
    prop_test2 t1 t2 = ds t1 t2 /= Nothing ==> t1 /= t2

    prop_test3 :: Term -> Term -> Property
    prop_test3 t1 t2 = ds t1 t2 == Nothing ==> isJust (unify t1 t2) && domain (fromJust (unify t1 t2)) == []

    prop_test4 :: Term -> Term -> Property
    prop_test4 t1 t2 = isJust (unify t1 t2) ==> ds (apply (fromJust (unify t1 t2)) t1) (apply (fromJust (unify t1 t2)) t2) == Nothing

    return []
    runTests :: IO Bool
    runTests = $quickCheckAll
