module Anviourment (start) where
    import Parser ( parseFile, parse )
    import Type ( Prog )
    import SLD ( Strategy, dfs, bfs, solveWith )
    import Pretty ( Pretty(pretty) )
    import Data.Maybe ( fromJust, isNothing ) 

    {-
        Prints help
    -}
    commandHelp :: IO ()
    commandHelp = do
        putStrLn "Commands available from the prompt:"
        putStrLn "  <goal>       Solves/proves the specified goal"
        putStrLn "  :h           Show this help text"
        putStrLn "  :l <path>    Loads a file"
        putStrLn "  :p           Prints the currently loaded program"
        putStrLn "  :q           Exists the environment"
        putStrLn "  :s <start>   Sets the given start. Available are 'bfs' and 'dfs'"

    {-
        Prints quit message
    -}
    commandQuit :: IO ()
    commandQuit = putStrLn "farewell"

    {-
        starts the interactive enviroment with default Strategy set to breadth-first-search
    -}
    start :: IO ()
    start = do
        putStrLn "G'Day, mate!\nType \":h\" for help."
        main Nothing bfs

    {-
        Read-Eval-Print-Loop
    -}
    main :: Maybe Prog -> Strategy -> IO ()
    main p s = do
        putStr "?- "
        r <- getLine
        case r of
            -- quit program (simply not calling main again)
            ":q" -> commandQuit

            -- Load file command
            ":l" -> do
                putStrLn "Syntax: :l <path>"
                main p s
            (':':'l':' ': xs) -> do
                fileRes <- parseFile xs
                case fileRes of
                    (Left err) -> 
                        do
                            putStrLn "Failed to parse file"
                            putStrLn err
                            main p s
                    (Right prog) -> do
                        putStrLn "Program loaded"
                        main (Just prog) s
            
            -- Print current program
            ":p" -> do
                if isNothing p
                    then do
                        putStrLn "No program loaded"
                        main p s
                    else do
                        putStrLn (pretty (fromJust p))
                        main p s

            -- Help command
            ":h" -> do
                commandHelp
                main p s

            -- Strategy command
            ":s" -> do
                putStrLn "Syntax: :s <strat>"
                main p s
            (':':'s':' ': strat) -> do
                case strat of
                    "bfs" -> do
                        putStrLn "Strategy set to breadth-first search"
                        main p bfs
                    "dfs" -> do
                        putStrLn "Strategy set to depth-first search"
                        main p dfs
                    _ -> do
                        putStrLn "Unknown strategy"
                        main p s
            
            -- If not one of the given command, parsing it as goal
            as -> do
                if isNothing p
                    then do
                        putStrLn "No program loaded"
                        main p s
                    else do
                        case parse as of
                            (Left err) -> do
                                putStrLn "Failed to parse goal:"
                                putStrLn err
                                main p s
                            (Right goal) -> do
                                let tree = solveWith (fromJust p) goal s
                                -- if length tree == 0
                                --     then do
                                --         putStrLn "No solutions."
                                --         main p s
                                --     else
                                inputLoop tree
                                    where
                                        -- inputLoop :: [String] -> IO()
                                        inputLoop [] = do
                                            putStrLn "No more solutions."
                                            main p s
                                        inputLoop (x:xs) = do
                                            putStr (pretty x)
                                            userIn <- getLine
                                            case userIn of
                                                "." -> do
                                                    main p s
                                                _ -> do
                                                    inputLoop xs
                                            
