{-# LANGUAGE TemplateHaskell #-}
module Rename (rename, runTests) where

    import Test.QuickCheck ( quickCheckAll, (==>), Property );
    import Type ( Rule(..), Term(..), VarName(..) );
    import Vars ( freshVars, Vars(allVars) );

    {-
        Counts and returns the number of anonymous variables
    -}
    findAllUnderscores :: Term -> Int -> Int
    findAllUnderscores (Var (VarName "_")) a = a + 1
    findAllUnderscores (Var _) a = a
    findAllUnderscores (Comb _ ts) a = foldr (\t b -> b + (findAllUnderscores t 0)) a ts

    {-
        Returns the first VarName from the second array that is not in the first
    -}
    firstNotUsed :: [VarName] -> [VarName] -> VarName
    firstNotUsed _ [] = undefined
    firstNotUsed [] (x:_) = x
    firstNotUsed ys (x:xs) = if x `elem` ys then firstNotUsed ys xs else x

    {-
        Creates an array with length 'n' and filled with given VarName
    -}
    constructArr :: Int -> VarName -> [VarName]
    constructArr 0 _ = []
    constructArr n e = e : constructArr (n-1) e

    -- forbidden Vars - Rule Term [Term] => Rule
    {-
        Renames all variables in given rule with new ones.
        All VarNames in given array will not be used as replacement
    -}
    rename :: [VarName] -> Rule -> Rule
    rename f (Rule t ts) = do
            let myVars = allVars (Rule t ts)
            let forbidden = f ++ myVars
            -- counts the amount of anonymous variables and adds just as many to the list of VarName's to change
            let anonymVarsCount = findAllUnderscores (Comb "!" (t : ts)) 0
            let tupel = rn forbidden (myVars ++ constructArr anonymVarsCount (VarName "_"))
            rename2 tupel (Rule t ts)
                where 
                    {-  
                        is given a list of forbidden VarName's and a list of VarName's to change.
                        Creates a tuple in which each VarName is assigned a new one
                    -}
                    rn :: [VarName] -> [VarName] -> [(VarName, VarName)]
                    rn _ [] = []
                    rn forb ocs = do
                                    let (c:cs) = ocs
                                    let n = firstNotUsed forb freshVars
                                    (c,n) : rn (forb ++ [n]) cs
        
                    {-
                        Changes the VarName's in a rule, using a tupel where each variable is
                        assigned a new one.
                        Pays patricular attention to the case VarName "_" since each "_" is
                        assigned a different VarName. 
                    -}
                    rename2 :: [(VarName, VarName)] -> Rule -> Rule
                    rename2 [] rule = rule
                    rename2 ((a,as): bs) (Rule t' ts')
                        | a == VarName "_" = do
                                            let (t1', res) = replace as t'
                                            if res
                                                then rename2 bs (Rule t1' ts')
                                                else
                                                    let (ts1', res1) = replaceArr as ts'
                                                    in if res1
                                                        then rename2 bs (Rule t' ts1') 
                                                        else rename2 bs (Rule t' ts')

                        | otherwise = rename2 bs (Rule (replaceInTerm a as t') (map (replaceInTerm a as) ts'))

                    {-
                        Case VarName "_"
                        replaces VarName "_" with the given VarName in a term and returns the
                        changed term and a Bool. 
                        The Bool is used to recognize if an anonymous variable has been replaced.
                    -}
                    replace :: VarName -> Term -> (Term, Bool)
                    replace e (Var (VarName "_")) = (Var e, True)
                    replace _ (Var vn) = (Var vn, False)
                    replace e (Comb cn ts') = do
                                            let (ter, res) = replaceArr e ts'
                                            (Comb cn ter, res)
                    {-
                        Case VarName "_"
                        Replaces only one anonymous variable in given terms with variable.
                        This method is necessary to stop the loop when one variable is replaced, this is not possible using 'map'
                    -}
                    replaceArr :: VarName -> [Term] -> ([Term], Bool)
                    replaceArr _ [] = ([], False)
                    replaceArr e (x:xs) = do
                        let (ter, res) = replace e x
                        if res then (ter : xs, True)
                        else do
                            let (ters, ress) = replaceArr e xs
                            (ter : ters, ress)

                    {-
                        Replaces all occurrences of the first VarName with the second one
                    -}
                    replaceInTerm :: VarName -> VarName -> Term -> Term
                    replaceInTerm v r (Var vn) = if v == vn then Var r else Var vn
                    replaceInTerm v r (Comb cn ts') = Comb cn (map (replaceInTerm v r) ts')

    -- start tests and utils-functions used by tests

    -- determines the intersection of two VarName arrays
    intersection :: [VarName] -> [VarName] -> [VarName]
    intersection x1 x2 = inter x1 x2 []
        where
            inter :: [VarName] -> [VarName] -> [VarName] -> [VarName]
            inter [] _ a = a
            inter (x : xs) x2' a
                | x `elem` x2' = inter xs x2' (a ++ [x])
                | otherwise = inter xs x2' a

    -- tests
    prop_test1 :: [VarName] -> Rule -> Bool
    prop_test1 xs r = intersection (allVars (rename xs r)) (allVars r) == []

    prop_test2 :: [VarName] -> Rule -> Bool
    prop_test2 xs r = intersection (allVars (rename xs r)) xs == []

    prop_test3 :: [VarName] -> Rule -> Bool
    prop_test3 xs r = not ((VarName "_") `elem` allVars (rename xs r))

    prop_test4 :: [VarName] -> Rule -> Property
    prop_test4 xs r = not ((VarName "_") `elem` allVars r) ==> length (allVars(rename xs r)) == length (allVars r)

    prop_test5 :: [VarName] -> Rule -> Bool
    prop_test5 xs r = length (allVars(rename xs r)) >= length (allVars r)

    return []
    runTests :: IO Bool
    runTests = $quickCheckAll