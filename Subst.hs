{-# LANGUAGE TemplateHaskell #-}
module Subst
-- wie nur dataentyp exportieren
(Subst, domain, empty, single, apply, compose, restrictTo, runTests)
 where

    import Type ( Term(..), VarName(..) );
    import Vars ( Vars(..) );
    import Pretty ( Pretty(..) )
    import Test.QuickCheck ( Arbitrary(arbitrary), quickCheckAll, frequency, (==>), Property )
    import Data.List ( sort )

    -- data type for Substitutions
    data Subst = Empty | Subst VarName Term Subst
        deriving Show

    -- returns all VarName's that project to a term in given subst
    domain :: Subst -> [VarName]
    domain Empty = []
    domain (Subst name _ arr) = do
        let subDom = domain arr
        if name `elem` subDom then subDom else name : subDom

    -- creates an empty Substitution
    empty :: Subst
    empty = Empty

    -- creates a substitution which only maps a single variable to a term
    single :: VarName -> Term -> Subst
    single v1 (Var v2) = if v1 == v2 then Empty else Subst v1 (Var v2) Empty
    single name term = Subst name term Empty

    -- applies a substitution to a term
    apply :: Subst -> Term -> Term
    apply Empty t = t
    apply (Subst subVar subTerm subs) (Var name) = if name == subVar then subTerm else apply subs (Var name)
    apply s (Comb cn ts) = Comb cn (map (apply s) ts)

    -- combines the seconds substitution with the first
    compose :: Subst -> Subst -> Subst
    compose Empty Empty = Empty
    compose Empty s = s
    compose s Empty = s
    compose a (Subst an at as) = removeDuplicateDomain (removeHeadIfEqual (Subst an (apply a at) (compose a as))) []
        where
            -- removes first subst of linked-subst-list if it's projecting to itself
            removeHeadIfEqual :: Subst -> Subst
            removeHeadIfEqual Empty = Empty
            removeHeadIfEqual (Subst an1 at1 as1) = if at1 == Var an1 then as1 else Subst an1 at1 as1

            -- removes a subst of linked-subst-list if a VarName is used multiple times
            removeDuplicateDomain :: Subst -> [VarName] -> Subst
            removeDuplicateDomain Empty _ = Empty
            removeDuplicateDomain (Subst an2 at2 as2) [] = Subst an2 at2 (removeDuplicateDomain as2 [an2])
            removeDuplicateDomain (Subst an2 at2 as2) vs = if an2 `elem` vs then removeDuplicateDomain as2 vs else Subst an2 at2 (removeDuplicateDomain as2 (an2:vs))

    -- removes all substitutions that replace a VariableName that is not in the given array
    restrictTo :: Subst -> [VarName] -> Subst
    restrictTo Empty _ = Empty
    restrictTo _ [] = Empty
    restrictTo (Subst subVar t subsubst) ar = if subVar `elem` ar  then Subst subVar t (restrictTo subsubst ar) else restrictTo subsubst ar

    -- returns pretty-printable Subst as string
    instance Pretty Subst where
        pretty s = "{" ++ prettyHelp s ++ "}"
            where
                prettyHelp Empty = ""
                prettyHelp (Subst (VarName vn) t Empty) = vn ++ " -> " ++ pretty t
                prettyHelp (Subst (VarName vn) t sub) = vn ++ " -> " ++ pretty t ++ ", " ++ prettyHelp sub

    -- returns all used VarName's of Subst
    instance Vars Subst where
        allVars Empty = []
        allVars (Subst vn t sub) = removeDuplicates (allVars sub ++ allVars t ++ [vn])
            where
                removeDuplicates :: [VarName] -> [VarName]
                removeDuplicates [] = []
                removeDuplicates (x:xs) = if x `elem` xs then removeDuplicates xs else x : removeDuplicates xs

    -- Generator for Subst
    instance Arbitrary Subst where
        arbitrary = do
            frequency [
                (4, empt),
                (2, rand)
                ]
            where
                empt = return Empty
                rand = do
                    name <- arbitrary
                    term <- arbitrary
                    sub <- arbitrary
                    return (restrictTo (Subst name term sub) (domain(Subst name term sub)))

    --from here: quickCheck_tests

    -- Utils-function to test for subsets
    isSubset :: [VarName] -> [VarName] -> Bool
    isSubset [] _ = True
    isSubset (x:xs) arr = x `elem` arr && isSubset xs arr

    addIfNotIn :: [VarName] -> VarName -> [VarName]
    addIfNotIn as b = if b `elem` as then as else as ++ [b]

    -- start tests
    prop_test1 :: Term -> Bool
    prop_test1 t = t == apply empty t

    prop_test2 :: VarName -> Term -> Bool
    prop_test2 x t = apply (single x t) (Var x) == t

    prop_test3 :: Term -> Subst -> Subst -> Bool
    prop_test3 t s1 s2 = apply (compose s1 s2) t == apply s1 (apply s2 t)

    prop_test4 :: Bool
    prop_test4 = domain empty == []

    prop_test5 :: VarName -> Bool
    prop_test5 x = domain (single x (Var x)) == []

    prop_test6 :: VarName -> Term -> Property
    prop_test6 x t = (t /= Var x) ==> domain (single x t) == [x]


    prop_test7 :: Subst -> Subst -> Bool
    prop_test7 s1 s2 = isSubset (domain (compose s1 s2)) (domain s1 ++ domain s2)

    prop_test8 :: VarName -> VarName -> Property
    prop_test8 x1 x2 = (x1 /= x2) ==> domain (compose (single x2 (Var x1)) (single x1 (Var x2))) == [x2]

    prop_test9 :: Bool
    prop_test9 = allVars empty == []

    prop_test10 :: VarName -> Bool
    prop_test10 x = allVars (single x (Var x)) == []

    prop_test11 :: VarName -> Term -> Property
    prop_test11 x t = (t /= Var x) ==> sort (allVars(single x t)) == sort(addIfNotIn (allVars t) x)

    prop_test12 :: Subst -> Subst -> Bool
    prop_test12 s1 s2 = isSubset (allVars (compose s1 s2)) (allVars s1 ++ allVars s2)

    prop_test13 :: VarName -> VarName -> Property
    prop_test13 x1 x2 = (x1 /= x2) ==> sort (allVars (compose (single x2 (Var x1)) (single x1 (Var x2)))) == sort[x1, x2]

    prop_test14 :: Subst -> Bool
    prop_test14 s = isSubset (domain s) (allVars s)

    prop_test15 :: [VarName] -> Bool
    prop_test15 xs = domain (restrictTo empty xs ) == []

    prop_test16 :: [VarName] -> Subst -> Bool
    prop_test16 xs s = isSubset (domain (restrictTo s xs)) xs

    return []
    runTests :: IO Bool
    runTests = $quickCheckAll
