{-# LANGUAGE TemplateHaskell #-}
module Teste where

    import Type;
    import Subst;
    import Vars;
    import Unification;
    import Data.Maybe;
    import Pretty;
    import Rename;

    import Parser;

    type Unificator = Maybe Subst
    data SLDTree = SLDTree Goal [(Unificator, SLDTree, Term)] | Success | Fail -- Reihenfolge unwichtig?
        deriving Show

    errTerm :: Term
    errTerm = Var (VarName "ErrorFail")

    tester :: Prog -> Goal -> SLDTree
    tester (Prog rules) (Goal []) = Success
    tester (Prog rules) (Goal (g0:terms)) = do
        -- error (foldr (\x y -> pretty x ++ y) "" (g0:terms))
        testerTerm (Prog rules) g0 (Goal terms)

    testerTerm :: Prog -> Term -> Goal -> SLDTree -- ([SLDTree], [Rule]) -- [(Term, Term)] -- 
    testerTerm (Prog rules) term (Goal gs) = do
        let forbiddenVars = goalVars (Goal gs)
        let renamedRules = renameRules rules []
        let workingRules = findWorkingRules renamedRules term
        let notWorking = nfindWorkingRules renamedRules term
        let trees = map (\(Rule rv rs) -> do
                            let subs = unify rv term
                            -- (rv, term)
                            (subs, tester (Prog rules) (Goal gs), rv)
                        ) renamedRules
        SLDTree (Goal (term:gs)) trees
        -- error (foldr (\x y -> pretty x ++ "\n" ++ y) "" workingRules)

        -- (trees, notWorking)
        -- trees
        -- notWorking
        -- let firstRule = findFirstWorkingRule renamedRules term
                
-- ?- append(X, Y, .(1, [])).
-- {B -> X, A -> .(1, []), Y -> .(1, [])}
-- >append(B, A, A)
--    Success

-- ?- append(X, Y, .(1, [])).
-- Nothing
--    Success



    findWorkingRules :: [Rule] -> Term -> [Rule]
    findWorkingRules [] _ = []
    findWorkingRules ((Rule rv rr):rs) t = do
        let uni = unify rv t
        if isNothing uni
            then findWorkingRules rs t
            else Rule rv rr : findWorkingRules rs t

    nfindWorkingRules :: [Rule] -> Term -> [Rule]
    nfindWorkingRules [] _ = []
    nfindWorkingRules ((Rule rv rr):rs) t = do
        let uni = unify rv t
        if isNothing uni
            then Rule rv rr : findWorkingRules rs t
            else findWorkingRules rs t

    goalVars :: Goal -> [VarName]
    goalVars (Goal []) = []
    goalVars (Goal (g0:g)) = allVars g0 ++ goalVars (Goal g)

    renameRules :: [Rule] -> [VarName] -> [Rule]
    renameRules [] _ = []
    renameRules (r:rs) forb = do
        let renamed = rename forb r
        let newForb = forb ++ allVars renamed
        renamed : renameRules rs newForb

    instance Pretty SLDTree where
        pretty = prettyHelp ""
            where
                prettyHelp :: String -> SLDTree -> String
                prettyHelp spacer Success = spacer ++ " Success"
                prettyHelp spacer Fail = spacer ++ " Fail"
                prettyHelp spacer (SLDTree (Goal []) arr) = spacer ++ "?- ."
                prettyHelp spacer (SLDTree g arr) = do
                    spacer ++ pretty g ++ ['\n'] ++ foldr (\(uni, tree, usedRule) b -> do
                        if (isNothing uni)
                            then spacer ++ "Nothing" ++ "\n" ++ prettyHelp (spacer ++ "  ") tree ++  b
                            else spacer ++ pretty (fromJust uni) ++ "\n" ++ spacer ++ ">" ++ pretty usedRule ++ "\n" ++ prettyHelp (spacer ++ "  ") tree ++ "\n" ++ b
                        ) "" arr ++ "\n"

    scriptToo :: Prog -> Goal -> SLDTree
    scriptToo (Prog rules) (Goal []) = Success
    scriptToo (Prog rules) (Goal goals) = do
        let ar = map (scriptTre (Prog rules)) goals
        SLDTree (Goal goals) ar

    scriptTre :: Prog -> Term -> (Unificator, SLDTree, Term)
    scriptTre (Prog rules) t = do
        let firstRule = findFirstWorkingRule rules t
        if isNothing firstRule
            then (Nothing, Fail, errTerm) -- This goal failed
            else do
                let (Rule rv rrs) = fromJust firstRule
                let uniPre = unify rv t
                if isNothing uniPre
                    then (uniPre, Fail, rv)
                    else do
                        let uni = fromJust uniPre
                        let newTerms = map (apply uni) rrs
                        -- let (SLDTree nextGoal nextArr) = script (Prog rules) (Goal newTerms)
                        let tree = SLDTree (Goal newTerms) [(Just uni, scriptToo (Prog rules) (Goal newTerms), rv)] -- ((Just uni, script (Prog rules) (Goal newTerms), rv) : nextArr)
                        (Just uni, tree, rv)

    isFail :: SLDTree -> Bool
    isFail Fail = True
    isFail _ = False


    isSucc :: SLDTree -> Bool
    isSucc Success = True
    isSucc _ = False

    isFact :: Rule -> Bool
    isFact (Rule _ []) = True
    isFact _ = False

    -- script :: Prog -> Goal -> SLDTree
    -- script (Prog rules) (Goal []) = Success
    -- -- script (Prog rules) (Goal goals) = do
    -- --     undefined 
    -- script (Prog rules) (Goal (g0:goals)) = do
    --     let firstRule = findFirstWorkingRule rules g0
    --     if isNothing firstRule
    --         then SLDTree (Goal []) [] -- goal failed before it even started; sad :(
    --         else do
    --             let (Rule rv rrs) = fromJust firstRule
    --             let uni = fromJust (unify rv g0)
    --             let newTerms = map (apply uni) rrs
    --             let rrr = script (Prog rules) (Goal goals)
    --             if isFail rrr
    --                 then SLDTree (Goal (g0 : goals)) ([(Just uni, script (Prog rules) (Goal newTerms), rv)])
    --                 else do
    --                     if isSucc rrr
    --                     then SLDTree (Goal (g0 : goals)) ([(Just uni, script (Prog rules) (Goal newTerms), rv)])
    --                         else do
    --                             let (SLDTree nextGoal nextArr) = rrr
    --                             SLDTree (Goal (g0 : goals)) ([(Just uni, script (Prog rules) (Goal newTerms), rv)] ++ nextArr )

    allGoalVars :: Goal -> [VarName]
    allGoalVars (Goal []) = []
    allGoalVars (Goal (x:xs)) = allVars x ++ allGoalVars (Goal xs)

    script :: Prog -> Goal -> SLDTree
    script (Prog rules) (Goal []) = Success
    script (Prog []) _ = Fail
    script (Prog rules) (Goal (g0:goals)) = do
        scriptTerm (Prog rules) g0 (Goal [g0])
        -- let res = map (\term -> do
        --             (Nothing, scriptTerm (Prog rules) term (Goal (g0:goals)), (Var (VarName "Err")))
        --             ) (g0:goals)
        -- SLDTree (Goal (g0:goals)) res

        -- let firstRule = findFirstWorkingRule rules g0
        -- if isNothing firstRule
        --     then SLDTree (Goal []) [] -- goal failed before it even started; sad :(
        --     else do
    
    scriptTerm :: Prog -> Term -> Goal -> SLDTree
    scriptTerm (Prog rules) term (Goal []) = undefined
    scriptTerm (Prog rules) term (Goal (g0:goals)) = do
        let renamedRules = renameRules rules (allGoalVars (Goal (g0:goals)))
        let workingRules = findWorkingRules renamedRules term
        if null workingRules
            then Fail
            else do
                let res = map (\r -> do
                        scriptRule (Prog workingRules) r (Goal [term])
                        ) [head workingRules]
                SLDTree (Goal (g0:goals)) res

    scriptRule :: Prog -> Rule -> Goal -> (Unificator, SLDTree, Term) -- [(Unificator, SLDTree, Term)]
    scriptRule _ (Rule rv _) (Goal []) = (Nothing, Success, rv)
    scriptRule (Prog rules) cRule (Goal (g0:goals)) = do
        if isFact cRule
            then do
                let (Rule rv _) = cRule
                (Nothing, Success, rv)
            else do
                let (Rule rv rrs) = cRule
                let uni = fromJust (unify rv g0)
                let newTerms = map (apply uni) rrs
                let rrr = script (Prog rules) (Goal goals)
                -- error (pretty rrr)
                if isFail rrr
                    then (Just uni, SLDTree (Goal (g0:goals)) [(Just uni, script (Prog rules) (Goal newTerms), rv)], rv)
                    else do
                        if isSucc rrr
                            then (Just uni, SLDTree (Goal (g0:goals)) [(Just uni, script (Prog rules) (Goal newTerms), rv)], rv)
                            else do
                                let (SLDTree nextGoal nextArr) = rrr
                                (Just uni, SLDTree (Goal (g0:goals)) ((Just uni, script (Prog rules) (Goal newTerms), rv) : nextArr), rv)


    -- script (Prog rules) (Goal (g0:goals)) = do
    --     -- let firstRule = findFirstWorkingRule rules g0
    --     let workingRules = findWorkingRules rules g0
    --     if null workingRules
    --         then Fail
    --         else do -- SLDTree (Goal (g0 : goals))
    --             let k = map (\firstRule -> do
    --                 -- if isNothing firstRule
    --                 --     then SLDTree (Goal []) [] -- goal failed before it even started; sad :(
    --                 --     else do
    --                         let (Rule rv rrs) = firstRule -- fromJust firstRule
    --                         let uni = fromJust (unify rv g0)
    --                         let newTerms = map (apply uni) rrs
    --                         let rrr = script (Prog rules) (Goal goals)
    --                         if isFail rrr
    --                             then (Just uni, firstRule, ([(Just uni, script (Prog rules) (Goal newTerms), rv)]))
    --                             else do
    --                                 if isSucc rrr
    --                                 then (Just uni, firstRule, [(Just uni, script (Prog rules) (Goal newTerms), rv)])
    --                                     else do
    --                                         let (SLDTree nextGoal nextArr) = rrr
    --                                         (Just uni, firstRule, [(Just uni, script (Prog rules) (Goal newTerms), rv)] ++ nextArr )
    --                         ) workingRules
    --             SLDTree (Goal (g0 : goals)) (map (\(uni, (Rule rv _), xsxx) -> do
    --                     (uni, SLDTree (Goal (g0:goals)) xsxx, rv)
    --                 ) k)
    -- doOneGoal :: Prog -> Term -> SLDTree
    -- doOneGoal (Prog rules) goal = do
    --     let firstRule = findFirstWorkingRule rules goal
    --     if isNothing firstRule
    --         then undefined -- This goal failed before it even started; sad :(
    --         else do
    --             let (Rule rv rrs) = fromJust firstRule
    --             let uni = fromJust (unify rv goal)
    --             let newTerms = map (apply uni) rrs
    --             SLDTree (Goal [goal]) [(Just uni, script (Prog rules) (Goal newTerms), rv)]

    -- returns the first rule that can be applied to the term
    findFirstWorkingRule :: [Rule] -> Term -> Maybe Rule
    findFirstWorkingRule [] _ = Nothing 
    findFirstWorkingRule ((Rule rv rr):rs) t = do
        let uni = unify rv t
        if isNothing uni
            then findFirstWorkingRule rs t
            else Just (Rule rv rr)

    -- run :: Unificator -> [Term] -> SLDTree
    -- run Nothing t = (Nothing, Fail, errTerm)
    -- run (Just subst) t = do
    --     let currentSubst = subst
        

-- ?- p(X), q(X).
-- {a -> X}
--   ?- .{b -> X}
--   ?- .


    -- test2 :: SLDTree
    -- test2 = map pretty (testerTerm
                -- (Prog [
                -- --      Rule (Comb "p" [(Var (VarName "a"))]) [],
                -- --      Rule (Comb "p" [(Var (VarName "b"))]) [],
                -- --      Rule (Comb "q" [(Var (VarName "b"))]) []
                --     Rule
                --     (Comb "p" [Var (VarName "X"), Var (VarName "Z")])
                --     [
                --         Comb "q" [Var (VarName "X"), Var (VarName "Y")],
                --         Comb "p" [Var (VarName "Y"), Var (VarName "Z")]
                --     ]
                --     ,
                --     Rule (Comb "p" [Var (VarName "X"), Var (VarName "X")]) [],
                --     Rule (Comb "q" [Var (VarName "a"), Var (VarName "b")]) []
                -- ])

                -- -- (Comb "p"
                -- --     [Var (VarName "S"), Var (VarName "b")]
                -- -- ))

                -- -- (Goal ([
                -- --     (Comb "p" [Var (VarName "X")]),
                -- --     (Comb "q" [Var (VarName "X")])
                -- -- ])))

                -- -- (Goal ([
                --     (Comb "p" [Var (VarName "S"), Var (VarName "b")])
                -- -- ]))

                -- (Goal [
                --     Comb "p" [Var (VarName "S"), Var (VarName "b")]
                -- ])
                -- )

    te :: Term -> [Rule] -> [(Unificator, SLDTree, Term)]
    te t [] = undefined -- No rules left to test
    te t ((Rule rv rrs):rs) = do
        let subs = unify rv t
        if isNothing subs 
            then -- the current rule can not be applied
                (subs, Fail, rv) : te t rs -- Lets try all other rules; also add the current fail
            else do -- it's possible to use this rule
                let rsubs = fromJust subs -- this Subst describes how to get from rule to input (t)
                let newTerms = rrs -- A list of all terms that we need to test
                if newTerms == []
                    then [(subs, Success, rv)] -- success -- this rule is a fact, meaning wen have a success :)
                    else do
                        let ruleArr = Rule rv rrs : rs
                        -- List of all results from all new terms
                        let tttt = map examp newTerms
                        let t = arrArr tttt [] -- examp undefined []
                        [(subs, SLDTree (Goal newTerms) t, rv)]
                            where
                                examp :: Term -> [(Unificator, SLDTree, Term)]
                                examp t = do
                                    let arrsst = te t (Rule rv rrs : rs)
                                    arrsst
                                arrArr :: [[a]] -> [a] -> [a]
                                arrArr [] ta = ta
                                arrArr (x:xs) ta = ta ++ x

    -- test :: SLDTree
    test = te
                (Comb "p"
                    [Var (VarName "S"), Var (VarName "b")]
                )

                ([
                    (Rule
                        (Comb "p" [Var (VarName "X"), Var (VarName "Z")])
                        [
                            Comb "q" [Var (VarName "X"), Var (VarName "Y")],
                            Comb "p" [Var (VarName "Y"), Var (VarName "Z")]
                        ]
                    ),
                    Rule (Comb "p" [Var (VarName "X"), Var (VarName "X")]) [],
                    Rule (Comb "q" [Var (VarName "a"), Var (VarName "b")]) []
                ])

    -- --import Test.QuickCheck;
    -- import Pretty;
    -- import Vars;

    -- findAllUnderscores :: Term -> Int -> Int
    -- findAllUnderscores (Var (VarName "_")) a = a + 1
    -- findAllUnderscores (Var _) a = a
    -- findAllUnderscores (Comb _ ts) a = foldr (\t b -> b + (findAllUnderscores t 0)) a ts

    -- firstNotUsed :: [VarName] -> [VarName] -> VarName
    -- firstNotUsed _ [] = undefined
    -- firstNotUsed [] (x:_) = x
    -- firstNotUsed ys (x:xs) = if x `elem` ys then firstNotUsed ys xs else x

    -- constructArr :: Int -> VarName -> [VarName]
    -- constructArr 0 _ = []
    -- constructArr n e = [e] ++ (constructArr (n-1) e)

    -- -- forbidden Vars - Rule Term [Term] => Rule
    -- rename :: [VarName] -> Rule -> Rule
    -- rename f (Rule t ts) = do
    --         let myVars = allVars (Rule t ts)
    --         let forbidden = f ++ myVars
    --         let u = (findAllUnderscores (Comb "!" ([t] ++ ts))) 0
    --         let tupel = rn forbidden (myVars ++  (constructArr u  (VarName "_")))
    --         rename2 tupel (Rule t ts)
    --             where 
    --                 -- forbidden, was ursprünglich im rule ist
    --                 rn :: [VarName] -> [VarName] -> [(VarName, VarName)]
    --                 rn _ [] = []
    --                 rn forb (c:cs) = let n = firstNotUsed forb freshVars
    --                     in [(c,n)] ++ rn (forb ++ [n]) cs
    --                 -- rn _ _ = error "löppt nicht"
        
    --                 rename2 :: [(VarName, VarName)] -> Rule -> Rule
    --                 rename2 [] rule = rule
    --                 rename2 ((a,as): bs) (Rule t' ts')
    --                     | a == VarName "_" = do
    --                                         let (t1', res) = replace as t'
    --                                         if res
    --                                             then rename2 bs (Rule t1' ts')
    --                                             else
    --                                                 let (ts1', res1) = replaceArr as ts'
    --                                                 in if res1
    --                                                     then rename2 bs (Rule t' ts1') 
    --                                                     else rename2 bs (Rule t' ts')

    --                     | otherwise = rename2 bs (Rule (replaceInTerm a as t') (map (replaceInTerm a as) ts'))


    --                 replace :: VarName -> Term -> (Term, Bool)
    --                 replace e (Var (VarName "_")) = (Var e, True)
    --                 replace _ (Var vn) = (Var vn, False)
    --                 replace e (Comb cn ts') = do
    --                                         let (ter, res) = replaceArr e ts'
    --                                         (Comb cn ter, res)

    --                 replaceArr :: VarName -> [Term] -> ([Term], Bool)
    --                 replaceArr _ [] = ([], False)
    --                 replaceArr e (x:xs) = do
    --                     let (ter, res) = replace e x
    --                     if (res) then ([ter] ++ xs, True)
    --                     else do
    --                         let (ters, ress) = replaceArr e xs
    --                         ([ter] ++ ters, ress)

    --                 replaceInTerm :: VarName -> VarName -> Term -> Term
    --                 --replaceInTerm _ _ (Var (VarName "_")) = (Var (VarName "_"))
    --                 replaceInTerm v r (Var vn) = if v == vn then (Var r) else (Var vn)
    --                 replaceInTerm v r (Comb cn ts') = Comb cn (map (replaceInTerm v r) ts')



    -- test3 :: SLDTree
    test3 = do
        let trees = (script (Prog [
                    Rule
                        (Comb "append" [Var (VarName "[]"), Var (VarName "Ys"), Var (VarName "Ys")]) [], 
                    Rule (Comb "append" [Comb "." [Var (VarName "X"), Var (VarName "Xs")],
                        Var (VarName "Ys"),
                        Comb "." [Var (VarName "X"), Var (VarName "Zs")]])
                        [Comb "append" [Var (VarName "Xs"), Var (VarName "Ys"), Var (VarName "Zs")]]
                        

                    ])
                    -- (Comb "append" [Var (VarName "X"), Var (VarName "Y"), Comb "." [Comb "1" [], Comb "[]" []]])
                    -- (Goal []))
                    (Goal [(Comb "append" [Var (VarName "X"), Var (VarName "Y"), Comb "." [Comb "1" [], Comb "[]" []]])]))
        -- map (\(t1, t2) -> (pretty t1, pretty t2)) terms
        -- (map pretty trees) ++ (map pretty nrules)
        pretty trees


    test5 = do
        let rv = (Comb "append" [Comb "." [Var (VarName "X"), Var (VarName "Xs")], Var (VarName "Ys"), Comb "." [Var (VarName "X"), Var (VarName "Zs")]])
        let query = (Comb "append" [Var (VarName "X"), Var (VarName "Y"), Comb "." [Comb "1" [], Comb "[]" []]])
        let (Rule rule _) = rename (allVars query) (Rule rv [])
        let s = empty
        let res = ds (apply s rule) (apply s query)
        let (t0, t1) = fromJust res
        (t0, t1)

    test6 = do
            pretty (tester (Prog [Rule
                (Comb "p" [Var (VarName "X"), Var (VarName "Z")])
                    [
                        Comb "q" [Var (VarName "X"), Var (VarName "Y")],
                        Comb "p" [Var (VarName "Y"), Var (VarName "Z")]
                    ], Rule (Comb "p" [Var (VarName "X"), Var (VarName "X")]) [], Rule (Comb "q" [Var (VarName "A"), Var (VarName "B")]) []])

                (Goal [(Comb "p"
                    [Var (VarName "S"), Var (VarName "B")]
                )]))

    test7 = do
        let prog = (Prog [
                        Rule (Comb "p" [Var (VarName "X"), Var (VarName "Z")]) [Comb "q" [Var (VarName "X"), Var (VarName "Y")], Comb "p" [Var (VarName "Y"), Var (VarName "Z")]],
                        Rule (Comb "p" [Var (VarName "X"), Var (VarName "X")]) [],
                        Rule (Comb "q" [Comb "a" [], Comb "b"[]]) []
                    ])
        let goal = (Goal [(Comb "p" [Var (VarName "S"), Comb "b" []])])
        pretty (try prog goal)

    try :: Prog -> Goal -> SLDTree
    try (Prog rules) (Goal []) = Success
    try (Prog []) _ = Fail
    try (Prog urules) (Goal (g0:goals)) = do
        let rules = renameRules urules (allVars (Goal (g0:goals)))
        let rule = findFirstWorkingRule rules g0
        if isNothing rule
            then Fail
            else do
                let (Rule rv rts) = fromJust rule
                let newTerms = rts
                let uni = unify g0 rv
                if isNothing uni
                    then Fail
                    else do
                        let otherGoals = map (apply (fromJust uni)) goals
                        let others = try (Prog rules) (Goal otherGoals)
                        if isFail others
                            then Fail -- SLDTree (Goal (g0:goals)) [(uni, Fail, errTerm)]
                            else do
                                if isSucc others
                                    then SLDTree (Goal (g0:goals)) ([(uni, try (Prog rules) (Goal newTerms), rv)] ++ [(Nothing, others, ((Var (VarName "Err"))))])
                                    else do
                                        let (SLDTree _ rrr) = others
                                        SLDTree (Goal (g0:goals)) ([(uni, try (Prog rules) (Goal newTerms), rv)] ++ rrr)
                        -- error ((foldr (\a b -> pretty a ++ b) "" otherGoals) ++ "\n" ++ (pretty rv) ++ "\n" ++ pretty (fromJust uni))