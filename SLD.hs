module SLD (sld, bfs, dfs, solveWith, Strategy) where

    import Type ( Goal(..), Prog(..), Rule(Rule), VarName );
    import Subst (apply, Subst, compose, restrictTo) --Wie auf Subst zugreifen wenn wir den Datentypen eig nicht exportieren dürfen
    import Unification ( unify );
    import Data.Maybe ( isJust, fromJust )
    import Vars ( Vars(allVars) )
    import Rename ( rename )

    type Unificator = Maybe Subst

    data SLDTree = SLDTree Goal [(Unificator, SLDTree)] -- Reihenfolge unwichtig?
        deriving Show

    {-
        Creates the SLDTree
    -}
    sld :: Prog -> Goal -> SLDTree
    sld (Prog []) goal = SLDTree goal []
    sld _ (Goal []) = SLDTree (Goal []) []
    sld (Prog rs) goal = createSLD (allVars goal) goal
        where
            createSLD :: [VarName] -> Goal -> SLDTree
            createSLD _ (Goal []) = SLDTree (Goal []) []
            createSLD vn (Goal (g : gs)) =
                let rules = map (rename vn) rs
                in SLDTree (Goal (g : gs)) [(u, a) | (Rule x ys) <- rules, 
                                                    let u = unify x g,
                                                    let a = createSLD (vn ++ concatMap allVars rules) (Goal (map (apply (fromJust u)) (ys ++ gs))),
                                                    isJust u]

    type Strategy = SLDTree -> [Subst]

    {-
        breadth-first search
    -}
    bfs :: Strategy
    bfs (SLDTree _ tree) = bfss tree
        where
            bfss :: [(Unificator, SLDTree)] -> [Subst]
            bfss [] = []
            bfss ((u, SLDTree (Goal []) ss) : ts) = fromJust u : bfss (ts ++ map (\(u2,t) -> (Just (compose (fromJust u2) (fromJust u)), t)) ss)
            bfss ((u, SLDTree _ ss) : ts) = bfss (ts ++ map (\(u2,t) -> (Just (compose (fromJust u2) (fromJust u)), t)) ss)

    {-
        depth-first-search
    -}
    dfs :: Strategy
    dfs (SLDTree _ tree) = dfss tree
        where
            dfss :: [(Unificator, SLDTree)] -> [Subst]
            dfss [] = []
            dfss ((u, SLDTree (Goal []) ss) : ts) = fromJust u : dfss (map (\(u2,t) -> (Just (compose (fromJust u2) (fromJust u)), t)) ss ++ ts)
            dfss ((u, SLDTree _ ss) : ts) = dfss (map (\(u2,t) -> (Just (compose (fromJust u2) (fromJust u)), t)) ss ++ ts)

    {-
        Returns all solutions for given program and goal with given strategy
    -}
    solveWith :: Prog -> Goal -> Strategy -> [Subst]
    solveWith p g s = do
        let solutions = s (sld p g)
        -- filter all variables that are not in the goal
        let filteredSolutions = map (\x -> restrictTo x (allVars g)) solutions
        filteredSolutions

-- instance Pretty SLDTree where
--     pretty = prettyHelp ""
--         where
--             prettyHelp :: String -> SLDTree -> String
--             prettyHelp spacer (SLDTree (Goal []) _) = spacer ++ "?- ."
--             prettyHelp spacer (SLDTree g arr) = do
--                 spacer ++ pretty g ++ ['\n'] ++ foldr (\(uni, tree) b -> do
--                                                     if (isNothing uni)
--                                                         then spacer ++ "Nothing" ++ "\n" ++ prettyHelp (spacer ++ "  ") tree ++  b
--                                                         else spacer ++ pretty (fromJust uni) ++ "\n" ++ spacer ++ ">" ++ "\n" ++ prettyHelp (spacer ++ "  ") tree ++ "\n" ++ b
--                                                     ) "" arr ++ "\n"