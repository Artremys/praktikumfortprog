module Vars (freshVars, allVars, Vars) where

    import Type ( Goal(..), Prog(..), Rule(..), Term(..), VarName(..) );
    
    -- removes duplicates
    removeDuplicates :: [VarName] -> [VarName]
    removeDuplicates [] = []
    removeDuplicates (x:xs) = if x `elem` xs then removeDuplicates xs else x : removeDuplicates xs

    -- returns an infinite list of VarName
    freshVars :: [VarName]
    freshVars = map VarName (
            map 
                (: []) 
                ['A'..'Z'] ++ [x : [n] | n <- ['0'..], x <- ['A' .. 'Z']]
        )

    -- class type for getting all variables used
    class Vars a where
        allVars :: a -> [VarName]

    -- returns all used VarName's of term 
    instance Vars Term where
        allVars (Var name) = [name]
        allVars (Comb _ []) = []
        allVars (Comb _ (x:xs)) = removeDuplicates (foldr(\x1 y -> allVars x1 ++ y) [] xs ++ allVars x)

    -- returns all used VarName's of rule
    instance Vars Rule where
         allVars (Rule t ts) = removeDuplicates (allVars t ++ foldr(\x y -> allVars x ++ y) [] ts)

    -- returns all used VarName's of prog
    instance Vars Prog where
        allVars (Prog a) = removeDuplicates (foldr(\x y -> allVars x ++ y) [] a)

    -- returns all used VarName's of goal
    instance Vars Goal where
        allVars (Goal a) = removeDuplicates (foldr(\x y -> allVars x ++ y) [] a)
